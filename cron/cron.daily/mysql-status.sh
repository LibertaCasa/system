#!/bin/sh
OUTPUT="nc -N 127.0.0.2 2424"
maximumSecondsBehind=20
/opt/mysql/bin/mysql -u repl-status -p'$dbmonpass' -e 'SHOW REPLICA STATUS \G' > /tmp/replicationstatus.txt

slaveRunning="$(cat /tmp/replicationstatus.txt | grep "Replica_IO_Running: Yes" | wc -l)"
slaveSQLRunning="$(cat /tmp/replicationstatus.txt | grep "Replica_SQL_Running: Yes" | wc -l)"
secondsBehind="$(cat /tmp/replicationstatus.txt | grep "Seconds_Behind_Source" | tr -dc '0-9')"

echo $slaveRunning | $OUTPUT
echo $slaveSQLRunning | $OUTPUT
echo $secondsBehind | $OUTPUT

if [[ $slaveRunning != 1 || $slaveSQLRunning != 1 || $secondsBehind -gt $maximumSecondsBehind ]]; then
  echo
  echo "Replikacja wydaje się być popieprzona. Sending logs via email. @cranberry" | $OUTPUT
  /usr/bin/mail -s "[MySQL Replication Monitor] Issue on $(hostname) at $(date)" system@lysergic.dev < /tmp/replicationstatus.txt
else
  echo
  echo "Replikacja wydaje się być zdrowa." | $OUTPUT
fi
