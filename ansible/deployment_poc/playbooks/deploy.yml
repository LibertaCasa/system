---
- hosts: status_planned
  gather_facts: no
  vars:
    token: "{{ nb_token }}"
    vm_name: "{{ inventory_hostname }}"
    tag_merged: []
    debug_merged: []
  vars_files:
    - ../variables/deploy-variables.yml

  pre_tasks:
    - name: Check lock
      wait_for:
        path: "{{ lockfile }}"
        state: absent
        timeout: 600
        msg: Lock did not disappear in time
      delegate_to: localhost
      
    - name: Create lock
      file:
        path:  "{{ lockfile }}"
        state: touch
      delegate_to: localhost

  tasks:
    - name: Pipeline
      block:
        - name: Gather details
          block:
            - import_tasks: "../tasks/netbox_query_vm.yml"
            - import_tasks: "../tasks/netbox_query_cluster.yml"
          no_log: true
        
        - name: Assign variables
          block:
            - import_tasks: "../tasks/netbox_evaluate_cluster.yml"
            - import_tasks: "../tasks/netbox_evaluate_vm.yml"
        
        - name: Verify compliance
          block:
            - name: Check status
              fail:
                msg: The object is not Planned.
              when: status != 'planned'
    
            - name: Check tag
              fail:
                msg: The object is marked as already being in deployment.
              when: '"active-deployment" in tags'

            - name: Check platform
              fail:
                msg: The object does not contain a valid platform attribute.
              when: os != 'openSUSE-Leap-x86_64' #support more OS's later

        - name: Write tag and journal
          import_tasks: "../tasks/netbox_tags_pre.yml"

        - name: Gather site configuration
          block:
            - import_tasks: "../tasks/netbox_query_site.yml"
            - import_tasks: "../tasks/netbox_evaluate_site.yml"
          no_log: true

        - name: Gather prefix
          block:
            - import_tasks: "../tasks/netbox_query_prefix.yml"
            - import_tasks: "../tasks/netbox_evaluate_prefix.yml"
          no_log: true

        - name: Gather IP address
          block:
            - import_tasks: "../tasks/netbox_query_ip.yml"
            - import_tasks: "../tasks/netbox_evaluate_ip.yml"
          no_log: true

        - name: Provision virtual machine
          import_tasks: "../tasks/configure_libvirt.yml"

        - name: Configure DHCP
          import_tasks: "../tasks/init_dhcp.yml"

        - name: Configure DNS
          import_tasks: "../tasks/init_dns.yml"

        - name: Configure Deployment Servers
          import_tasks: "../tasks/init_dps.yml"

        - name: Create interface object in NetBox or use existing one
          block:
            - import_tasks: "../tasks/netbox_init_interface.yml"
            - import_tasks: "../tasks/netbox_query_interface.yml"
            - import_tasks: "../tasks/netbox_evaluate_interface.yml"
          no_log: true

        - name: Define IP address object in NetBox
          block:
            - import_tasks: "../tasks/netbox_init_ip.yml"
            - import_tasks: "../tasks/netbox_primaryip.yml"
          no_log: true

        - name: Start VM and attach console
          import_tasks: "../tasks/init_vm_console.yml"

        - name: Initialize SSH CA
          import_tasks: "../tasks/init_ssh.yml"

        - name: Assist guest OS installation
          import_tasks: "../tasks/autoyast_assistant.yml"

        - name: Wait for guest OS installation
          import_tasks: "../tasks/wait.yml"

        - name: Configure SSH
          import_tasks: "../tasks/configure_ssh.yml"

  
      always:
        - name: Restore original tags
          import_tasks: "../tasks/netbox_tags_post.yml"
    
        - name: Remove lock
          file:
            path: "{{ lockfile }}"
            state: absent
          delegate_to: localhost
     
        - name: Debug
          ansible.builtin.debug:
            msg: "{{ status if status is defined}} - {{ tags if tags is defined }} - {{ host if host is defined }} - {{ host_status if host_status is defined }} - {{ namespace if namespace is defined }} - {{ os if os is defined }} - {{ vcpus if vcpus is defined }} - {{ memory if memory is defined }} - {{ disk if disk is defined }}"

