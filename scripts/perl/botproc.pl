#!/usr/bin/perl

# Work in progress.
# Requires botproc.ini.

use Config::Tiny;
use Net::OpenSSH;
use warnings;
use strict;
#use feature qw(say);

my $config = Config::Tiny->new;
$config = Config::Tiny->read( 'botproc.ini' );

foreach my $section (keys %{$config}) {
        my $host = "$section";
        my $OS  = $config->{$section}->{OS}; 
        #print 'The OS of ', $host, ' is ', $OS, "\n";
        my $user = $config->{$section}->{User};
        my $keyname = $config->{$section}->{Key};
        my $keypath = "/home/georg/.ssh/" . $keyname;
        my $port = $config->{$section}->{Port};
        print 'Connecting to ', $host, ':', $port, ' as ', $user, ' using key ', $keyname, "\n";
        my $ssh = Net::OpenSSH->new($host, user => $user, port => $port, key_path => $keypath);
        $ssh->error and
                die "FATAL: ", $ssh->error;
        $ssh->system("uname -a") or
                die "Remote command failed: ", $ssh->error;
        my ($df, $err) = $ssh->pipe_out("df -h /") or
                die "df query failed: " . $ssh->error;
        print "Root Partition:\n";
        while (<$df>) { print }
        close $df;
}
