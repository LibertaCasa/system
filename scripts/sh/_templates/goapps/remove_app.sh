#!/bin/sh

# Check for existing installation in `/opt`.

if [ ! -d /opt/app ]
then
echo "This will kill running app services and cause data loss! Remove app? "
read text -r

# https://github.com/koalaman/shellcheck/wiki/SC3015
# https://github.com/koalaman/shellcheck/wiki/SC2003
# Need a POSIX compatible regex soln below!
expr "$text" : "^[Yy]$" > /dev/null
if [ "$text" ] 
then
echo "Removing app"
systemctl disable --now app.service || true
rm -f /etc/systemd/system/app.service
rm -rf /opt/app
userdel -f app
groupdel -f app
echo "OK"
fi
else
echo "Could not find a compatible installation of app."
fi
