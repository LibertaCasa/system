#!/bin/sh
# Easily update Tor routers on NetBSD to the source release version specified as VERSION
# Combine this with multi.sh \o/

set -eu

if test `uname` = 'NetBSD'; then

VERSION="0.4.6.7"
TEMPDIR="~root/tmp"

BINARY="/usr/local/bin/tor"
check_version() {
        $BINARY --version | sed 's/[^0-9.]*\([0-9.]*\).*/\1/' | head -n 1
}

if test `check_version` != "$VERSION."; then

if ! test -d "$TEMPDIR"; then
mkdir $TEMPDIR
fi
cd $TEMPDIR

echo "Downloading ..."
curl -sLO https://dist.torproject.org/tor-$VERSION.tar.gz

echo "Extracting ..."
tar -xzf tor-$VERSION.tar.gz

echo "Configuring ..."
cd tor-$VERSION
./configure

echo "Backing up ..."
tar -czf /tmp/tor-`hostname -s`.tgz /etc/rc.d/tor /usr/local/etc/tor/torrc /var/chroot/tor

echo "Stopping existing instances ..."
set +e
/etc/rc.d/tor stop
sleep 2
pkill -9 tor
set -e

echo "Removing shit ..."
make uninstall

echo "Compiling ..."
# to-do
#make -j `sysctl -n hw.ncpu`
make

echo "Installing ..."
make install

echo "Starting new instance ..."
/etc/rc.d/tor start

else
        echo "Version matches, aborting."
        exit 1
fi

else
        echo "This script only supports NetBSD deployments."
        exit 1
fi
