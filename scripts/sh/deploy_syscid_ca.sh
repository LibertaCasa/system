#!/bin/sh
if [ "$(id -u)" = "0" ]; then
DISTRIB=$(awk -F= '/^NAME/{print $2}' /etc/os-release)
echo "Detected $DISTRIB"
CRT="/tmp/syscid-ca.crt"
URL="https://web.syscid.com/syscid-ca.crt"
FP_TRUST="SHA512 Fingerprint=53:85:72:A7:39:80:42:5B:54:19:7A:28:E8:A4:60:CE:6F:F3:D8:87:0D:6A:27:DE:00:05:C4:52:96:5B:BF:CB:BE:8D:39:C8:C3:76:59:8C:9C:DE:4C:C7:E8:10:1C:B9:6E:AF:82:36:5E:51:43:B3:B7:AE:B8:54:84:B8:4A:07"
if [ "${DISTRIB}" = '"openSUSE Leap"' ] || [ "${DISTRIB}" = '"openSUSE Tumbleweed"' ]; then
	STORE="/etc/pki/trust/anchors/"
	/usr/bin/curl -kso $CRT $URL
	FP_CRT=$(/usr/bin/openssl x509 -fingerprint -sha512 -noout -in $CRT)
	if [ "$FP_CRT" = "$FP_TRUST" ]; then
		echo "OK, installing..."
		mv $CRT $STORE
		/usr/sbin/update-ca-certificates
		echo "OK"
	else
		echo "Fingerpring mismatch. Operation aborted."
		rm -f $CRT
	fi
elif [ "${DISTRIB}" = '"Arch Linux"' ]; then
	OS="Arch"
	echo $OS
	/usr/bin/curl -kso $CRT $URL
	FP_CRT=$(/usr/bin/openssl x509 -fingerprint -sha512 -noout -in $CRT)
	if [ "$FP_CRT" = "$FP_TRUST" ]; then
		echo "OK, installing..."
		/usr/bin/trust anchor --store $CRT
		rm $CRT
		echo "OK"
	else
		echo "Fingerpring mismatch. Operation aborted."
		rm -f $CRT
	fi
else
	echo "Unsupported operating system."
fi
else
echo "This script must be run with root privileges."
fi
