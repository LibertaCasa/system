#!/bin/sh
# Created 10/01/2022 by Georg Pfuetzenreuter <georg@lysergic.dev>
# Simple script to clone and build the latest OVMF firmware
case "$1" in
	"fork" ) repo_owner="tacerus" ;;
	"upstream" ) repo_owner="tianocore" ;;
	* ) echo "Specify repository owner (fork - tacerus or upstream - tianocore)." && exit 1 ;;
esac
dir="edk2-git-$repo_owner"
git clone "https://github.com/$repo_owner/edk2.git" "$dir"
cd "$dir"
git submodule init
git submodule update --recursive
set --
. ./edksetup.sh
cat << 'EOF' > Conf/target.txt
ACTIVE_PLATFORM       = OvmfPkg/OvmfPkgX64.dsc
TARGET                = RELEASE
TARGET_ARCH           = X64
TOOL_CHAIN_CONF       = Conf/tools_def.txt
TOOL_CHAIN_TAG        = GCC5
BUILD_RULE_CONF = Conf/build_rule.txt
EOF
sed \
	-e 's/DEFINE NETWORK_HTTP_BOOT_ENABLE       = FALSE/DEFINE NETWORK_HTTP_BOOT_ENABLE       = TRUE/' \
	-e 's/DEFINE NETWORK_ISCSI_ENABLE           = TRUE/DEFINE NETWORK_ISCSI_ENABLE           = FALSE/' \
	-e 's/DEFINE PVSCSI_ENABLE           = TRUE/DEFINE PVSCSI_ENABLE           = FALSE/' \
	-e 's/DEFINE MPT_SCSI_ENABLE         = TRUE/DEFINE MPT_SCSI_ENABLE         = FALSE/' \
	OvmfPkg/OvmfPkgX64.dsc > OvmfPkg/OvmfPkgX64.dsc.custom
cp OvmfPkg/OvmfPkgX64.dsc.custom OvmfPkg/OvmfPkgX64.dsc
sed \
	-e 's#\"-Werror\"#//\"-Werror\"#' \ 
	BaseTools/Source/C/BrotliCompress/brotli/BUILD > BaseTools/Source/C/BrotliCompress/brotli/BUILD.custom
cp BaseTools/Source/C/BrotliCompress/brotli/BUILD.custom BaseTools/Source/C/BrotliCompress/brotli/BUILD
sed \
	-e 's/-Werror//' \
	BaseTools/Source/C/Makefiles/header.makefile > BaseTools/Source/C/Makefiles/header.makefile.custom
cp BaseTools/Source/C/Makefiles/header.makefile.custom BaseTools/Source/C/Makefiles/header.makefile
make -j$(nproc) -C BaseTools/Source/C
build
if [ -f "Build/OvmfX64/RELEASE_GCC5/FV/OVMF.fd" ]
then
	echo "OK, firmware is at:"
	realpath "Build/OvmfX64/RELEASE_GCC5/FV/OVMF.fd" 
else
	echo "Failed, no firmware file."
fi
if [ -f "Build/OvmfX64/RELEASE_GCC5/FV/OVMF_VARS.fd" ]
then
	echo  "OK, var template is at:"
	realpath "Build/OvmfX64/RELEASE_GCC5/FV/OVMF_VARS.fd"
else
	echo "Failed, no vars file."
fi
echo "Done."
