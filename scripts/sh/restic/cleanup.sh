#!/bin/sh
. restic_env
/usr/bin/restic forget --group-by hosts,paths --keep-daily 90 --prune --cleanup-cache -v
