#!/bin/sh
#/usr/bin/echo Executing Restic S3 Backup for $(hostname -f)
#Note: This script includes a special configuration for orpheus to include Gitea dumps in the backup and hence differs from the universal scripts deployed to other machines.
export RESTIC_REPOSITORY="s3:$S3PROTO://$S3HOST/$S3BUCKET"
export RESTIC_PASSWORD_FILE="$LIBPASS"
export AWS_ACCESS_KEY_ID="$S3AKI"
export AWS_SECRET_ACCESS_KEY="$S3SAK"
export RESTIC_CACHE_DIR="$CACHE"
EXCLUDE=".restic.excludes"
echo "Creating Gitea dump prior to backup ..."
GITEA_DUMPFILE="gitea-$(date -u +%d%m%y-%H%M).tar.gz"
sudo -u git /usr/local/bin/gitea dump -L --type tar.gz -t /var/lib/git/dump -c /etc/gitea/app.ini -C /var/lib/gitea/custom -w /var/lib/gitea -f /var/lib/git/dump/$GITEA_DUMPFILE
echo "Created Gitea Dumpfile: $GITEA_DUMPFILE"
echo
echo "Storing /etc/ ..."
/usr/bin/restic --verbose backup --exclude-file="/etc/$EXCLUDE" /etc
echo "EXIT $?"
echo "Storing /home/ ..."
/usr/bin/restic --verbose backup --exclude-file="/home/$EXCLUDE" /home
echo "EXIT $?"
echo "Storing /mnt/gluster01/ ..."
/usr/bin/restic --verbose backup --exclude-file="/mnt/gluster01/$EXCLUDE" /mnt/gluster01
echo "EXIT $?"
echo "Storing /opt/ ..."
/usr/bin/restic --verbose backup --exclude-file="/opt/$EXCLUDE" /opt
echo "EXIT $?"
echo "Storing /srv/ ..."
/usr/bin/restic --verbose backup --exclude-file="/srv/$EXCLUDE" /srv
echo "EXIT $?"
echo "Storing /var/ ..."
/usr/bin/restic --verbose backup --exclude-file="/var/$EXCLUDE" /var
echo "EXIT $?"
echo "Removing Gitea dumpfile."
sudo -u git /usr/bin/rm /var/lib/git/dump/$GITEA_DUMPFILE
echo
echo "OK"
