#!/bin/sh

menu() {
        echo "1) Domain XML"
        echo "2) Volume XML (not yet implemented)"
        echo "3) Network XML (not yet implemented"
        echo "x) Exit"
        echo
}

selection() {
        local selection
        echo "Enter [1|2|3|x] "
        read selection
        case $selection in
                1) domain ;;
                2) volume ;;
                3) network ;;
                x) 
                echo "Aborting on user request."
                exit 0
                ;;
                *) echo -e "${RED}Invalid input.${STD}"
        esac
}

#trap '' SIGINT SIGQUIT SIGSTP

domain() {
        local name
        local storelocation
        local store
        local storename
        echo "Name of the new domain: "
        read name
        echo "Storage location of the disk: /mnt/"
        read storelocation
        echo "Storage name of the pool: /mnt/$storelocation/"
        read store
        echo
        echo "Name: $name"
        echo "Disk: /mnt/$storelocation/$store/$name.qcow2"
        echo "Correct? [y|n|x] "
        read confirmation
        case $confirmation in
                y | yes) echo "OK" ;;
                n | no) echo "Starting over" && domain ;;
                x | menu) menu
        esac

        sed -e "s/%%NAME%%/$name/" -e "s/%%STORELOCATION%%/$storelocation/" -e "s/%%STORE%%/$store/" template.xml > $name.xml

        echo "Created $name.xml"
        exit 1
}


while true
do
        menu
        selection
done

