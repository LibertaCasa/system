#!/bin/sh
if [ -d /etc/systemd/system ]
then
if [ ! -d /opt/node_exporter ]
then
groupadd -g 501 prometheus
useradd -rd /opt/node_exporter -g 501 -u 501 -s /bin/false node_exporter
mkdir /opt/node_exporter
curl -Lo /opt/node_exporter/LICENSE 192.168.0.120/opt/node_exporter/LICENSE
curl -Lo /opt/node_exporter/NOTICE 192.168.0.120/opt/node_exporter/NOTICE
curl -Lo /opt/node_exporter/node_exporter 192.168.0.120/opt/node_exporter/node_exporter
chmod +x /opt/node_exporter/node_exporter
chown -R node_exporter:prometheus /opt/node_exporter
chmod 750 /opt/node_exporter
cat <<'EOF' >/etc/systemd/system/node_exporter.service
[Unit]
Description=Prometheus Node Exporter
After=network.target

[Service]
User=node_exporter
Group=prometheus
Type=simple
ExecStart=/opt/node_exporter/node_exporter --web.listen-address=":9200"

[Install]
WantedBy=multi-user.target
EOF
systemctl enable --now node_exporter
if [ -x $(command -v firewall-cmd) ] 
then
firewall-cmd --zone=internal --add-port=9200/tcp --permanent
firewall-cmd --reload
else
echo "No firewalld, skipping."
fi
echo "OK"
else
echo "Found traces of an existing installation. Aborted."
fi
else
echo "Not compatible with this system. No systemd?"
fi
