#!/bin/sh
GOVER=`go version | { read _ _ v _; echo ${v#go}; }`
echo "Detected Go $GOVER"
GOVER_REQ="1.16.0"
if [ "$(printf '%s\n' "$GOVER_REQ" "$GOVER" | sort -V | head -n1)" = "$GOVER_REQ" ]
then
if [ -f /opt/ergo/ergo-git/ergo ]
then
read -p "Update Ergo on this system? This will immediately stop Ergo if it is running. " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
systemctl stop ergo.service
cd /opt/ergo/ergo-git
rm -f ergo
git pull
sudo -u ergo make build
sudo -u ergo /opt/ergo/ergo-git/ergo upgradedb --conf /opt/ergo/ircd.yaml
systemctl start ergo.service
fi
else
echo "Ergo does not seem to be installed. Consider deploy_ergo.sh."
fi
else
echo "This is currently only compatible with Go $GOVER_REQ or higher. Consider deploy_go.sh."
fi
