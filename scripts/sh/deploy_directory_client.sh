#!/bin/sh
echo "You have 10 seconds to abort with Ctrl+C."
sleep 10s
if [ "$(id -u)" = "0" ]; then
DISTRIB=$(awk -F= '/^NAME/{print $2}' /etc/os-release)
if [ "${DISTRIB}" = '"openSUSE Leap"' ] || [ "${DISTRIB}" = '"openSUSE Tumbleweed"' ]; then
if [ -f /etc/pki/trust/anchors/syscid-ca.crt ]; then
	echo "OK, enrolling client ..."
	zypper in --no-recommends -y sssd sssd-ldap sssd-tools
	sed -i "s/NETCONFIG_DNS_STATIC_SERVERS=.*/NETCONFIG_DNS_STATIC_SERVERS=\"192.168.0.115 10.0.0.1\"/g" /etc/sysconfig/network/config
	netconfig update -f
	mv /etc/sssd/sssd.conf /etc/sssd/sssd.conf.orig
	cat <<'EOF' >/etc/sssd/sssd.conf
# SYSCID Directory and Authentication Service
# System Security Services Daemon configuration
# 12/08/2021 - georg@lysergic.dev
#
# WARNING - DEBUG LOGGING IS ENABLED
#
[sssd]
debug_level = 5
config_file_version = 2
services = nss, pam, ssh, sudo
domains = SYSCID

[nss]
debug_level = 5
homedir_substring = /home

[pam]
debug_level = 5
pam_pwd_expiration_warning = 1
pam_account_expired_message = Permission denied - Your SYSCID or LibertaCasa Account EXPIRED.
pam_account_locked_message = Permission denied - Your SYSCID or LibertaCasa Account is LOCKED.

[ssh]

[sudo]

[domain/SYSCID]
ignore_group_members = False
debug_level = 10
cache_credentials= False
id_provider = ldap
auth_provider = ldap
access_provider = ldap
chpass_provider = ldap
ldap_schema = rfc2307bis
ldap_search_base = dc=syscid,dc=com
ldap_uri = ldaps://ldap.syscid.com
ldap_access_filter = (memberOf=cn=syscid_shell_users,ou=syscid-groups,dc=syscid,dc=com)
access_provider = ldap
ldap_user_member_of = memberof
ldap_user_gecos = cn
ldap_user_uuid = nsUniqueId
ldap_group_uuid = nsUniqueId
ldap_account_expire_policy = rhds
ldap_access_order = filter, expire, pwd_expire_policy_renew
ldap_user_ssh_public_key = sshPublicKey
sudo_provider = ldap
ldap_sudo_search_base = ou=SUDOers,ou=syscid-system,dc=syscid,dc=com
EOF
	chmod 600 /etc/sssd/sssd.conf
	mv /etc/nsswitch.conf /etc/nsswitch.conf.orig
	cat <<'EOF' >/etc/nsswitch.conf
# SYSCID Directory and Authentication Service
# Name Service Switch configuration
# 12/08/2021 - georg@lysergic.dev
#
passwd:         sss files
group:          sss files
shadow:         sss compat
hosts:          files dns
networks:       files dns
aliases:        files usrfiles
ethers:         files usrfiles
gshadow:        files usrfiles
netgroup:       files nis
protocols:      files usrfiles
publickey:      files
rpc:            files usrfiles
services:       files usrfiles
automount:      files nis
bootparams:     files
netmasks:       files
sudoers:        sss
EOF
	mv /etc/ssh/sshd_config /etc/ssh/sshd_config_local
	cat <<'EOF' >/etc/ssh/sshd_config
# SYSCID Directory and Authentication Service
# OpenSSH Daemon configuration
# 12/08/2021 - georg@lysergic.dev
#
# WARNING - DEBUG LOGGING IS ENABLED
#
Port 28
Protocol 2
SyslogFacility AUTH
LogLevel VERBOSE
LoginGraceTime 1m
PermitRootLogin no
StrictModes yes
MaxAuthTries 5
MaxSessions 10
PubkeyAuthentication yes
AuthorizedKeysFile      	/etc/ssh/keys/%u
AuthorizedKeysCommand           /usr/bin/sh -c '/usr/bin/sss_ssh_authorizedkeys %u'
AuthorizedKeysCommandUser       nobody
PasswordAuthentication no
ChallengeResponseAuthentication no
UsePAM yes
X11Forwarding yes
PrintMotd yes
PrintLastLog yes
Banner /etc/ssh/sshd-banner
Subsystem       sftp    /usr/lib/ssh/sftp-server
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL
EOF
	pam-config -a --sss --mkhomedir
	systemctl enable --now sssd.service
	echo "OK!"
else
	echo "CA certificate not installed. Aborted. Consider 'deploy_syscid_ca.sh'."
fi
else
	echo "Unsupported operating system."
fi
else
	echo "This script must be run with root privileges."
fi
