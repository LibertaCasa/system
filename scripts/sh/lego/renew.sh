#!/bin/sh
OUTPUT="nc -N 127.0.0.2 2424"
echo "#universe ====================" | $OUTPUT
echo "#universe Starting daily certificate check ..." | $OUTPUT
. /opt/lego/pdns.env
for domain in $(/opt/lego/lego --path /etc/ssl/lego list |grep "Certificate Name:" |cut -d ":" -f 2)
do
        if [ -f /opt/lego/hooks/$domain.sh ]
        then
                /opt/lego/lego -a -m="system@lysergic.dev" -k ec384 --path /etc/ssl/lego --dns.resolvers 127.0.0.3 --dns pdns -d $domain renew --days 45 --renew-hook="/opt/lego/hooks/$domain.sh" |& $OUTPUT
        else
                /opt/lego/lego -a -m="system@lysergic.dev" -k ec384 --path /etc/ssl/lego --dns.resolvers 127.0.0.3 --dns pdns -d $domain renew --days 45 |& $OUTPUT
        fi

done
echo "#universe OK, finished daily certificate run." | $OUTPUT
echo "#universe ====================" | $OUTPUT
