#!/bin/sh
# Simple way to ensure a Salt minion's key matches before accepting it
# Run `salt-call --local key.finger` on the minion and paste the output once prompted (this script should be run on the Salt master)
# Georg Pfuetzenreuter <georg@lysergic.dev>
set -Ceu

minion="${1:-null}"
NOCOLOR=`tput sgr0`

if [ "$minion" = 'null' ]
then
        printf 'Please specify the minion to diff on\n'
        exit 1
fi

key_salt=`salt-key --out json -f "$minion" | jq --arg minion "$minion" -r '.minions_pre[$minion]'`

printf 'Enter fingerprint to diff against\n'
read key_user


if [ "$key_salt" = "$key_user" ]
then
        GREEN=`tput setaf 2`
        printf '%sMatches%s\n' "$GREEN" "$NOCOLOR"
        salt-key -a "$minion"
elif [ ! "$key_salt" = "$key_user" ]
then
        RED=`tput setaf 1`
        printf '%sMismatch%s\n' "$RED" "$NOCOLOR"
        exit 2
fi
