#!/bin/sh
DATE=$(date +'%d-%m-%Y')
echo $DATE > /mnt/backup/xtrabackup/current_basedate
/usr/bin/mkdir /mnt/backup/xtrabackup/full/$DATE
/opt/xtrabackup/bin/xtrabackup --backup --target-dir=/mnt/backup/xtrabackup/full/$DATE |& nc -N 127.0.0.2 2424
