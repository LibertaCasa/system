#!/bin/sh
DATE=$(date +'%d-%m-%Y')
BASEDATE=$(cat /mnt/backup/xtrabackup/current_basedate)
echo "INCREMENTAL: $DATE"
echo "BASE: $BASEDATE"
/usr/bin/mkdir /mnt/backup/xtrabackup/incremental/$DATE
/opt/xtrabackup/bin/xtrabackup --backup --target-dir=/mnt/backup/xtrabackup/incremental/$DATE --incremental-basedir=/mnt/backup/xtrabackup/full/$BASEDATE |& nc -N 127.0.0.2 2424
