#!/bin/bash

PDNSGROUP="pdns"

if id -nG $(id -un) | grep -qw "$PDNSGROUP"; then

if [ $# -eq 0 ] || [ $1 == "help" ] || [ $1 == "h" ] || [ $1 == "-h" ] || [ $1 == "-help" ] || [ $1 == "--help" ]; then
	echo "======================================================="
	echo "This script will create and secure a new zone in the Lysergic PowerDNS cluster."
	echo "Please run it by specifying the desired domain name as an argument."
	echo "Syntax: $ bash domain.sh example.com"
	echo "Warning: No syntax check will be performed. Make sure you type the domain name exactly right or you'll cause a deep fucking mess."
	echo "======================================================="
	exit 1
fi

else
	echo "You do not belong to the group $PDNSGROUP and hence are not allowed to run this program."
	echo "Assign yourself to the group or contact someone who is authorized to do so."
	exit 1
fi

DOMAIN=$1
NS1="ns1.3zy.de"
NS2="ns2.3zy.de"
NS3="ns3.3zy.de"

echo "==================================================="
echo "===== CREATING ZONE FOR DOMAIN $DOMAIN ====="
echo "==================================================="
/usr/bin/pdnsutil create-zone $DOMAIN $NS1
#/usr/bin/pdnsutil add-record $DOMAIN . SOA '$NS1 system.lysergic.dev 1 010800 3600 604800 3600'
#/usr/bin/pdnsutil add-record $DOMAIN . NS $NS1
/usr/bin/pdnsutil add-record $DOMAIN . NS $NS2
/usr/bin/pdnsutil add-record $DOMAIN . NS $NS3
echo "==================================================="
echo "================= ENABLING DNSSEC ================="
echo "==================================================="
/usr/bin/pdnsutil secure-zone $DOMAIN
/usr/bin/pdnsutil set-nsec3 $DOMAIN '1 0 1 ab'
/usr/bin/pdnsutil rectify-zone $DOMAIN
echo "==================================================="
/usr/bin/pdnsutil show-zone $DOMAIN
echo "==================================================="
/usr/bin/pdnsutil list-zone $DOMAIN
echo "==================================================="

