function jqless --description 'alias jqless jq -C . | less -R'
    jq -C . | less -R $argv
end
