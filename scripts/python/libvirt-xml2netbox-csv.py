#!/usr/bin/python3
"""
Takes a directory with libvirt XML domain dumps and creates a NetBox importable CSV. Since the role and platform fields cannot be fed from the XML files, the user is given the option to pick those for each VM.

Created and Last modified: 02/01/2022 by Georg Pfuetzenreuter <georg@lysergic.dev>
"""
import os
import xml.etree.ElementTree as xmlet
import pandas
import math

cluster = 'xxx'
tenant = 'xxx'

outfile = cluster + '.csv'

columns = [ "name", "status", "role", "cluster", "tenant", "platform", "vcpus", "memory", "disk", "comments" ]
rows = []

domaindir = 'xmls/' + cluster + '/domains'
diskdir = 'xmls/' + cluster + '/disks'

status = 'active'
comment = 'Imported from libvirt.'

for domainxml in os.listdir(domaindir):
    domainparse = xmlet.parse(domaindir + "/" + domainxml)
    domainroot = domainparse.getroot()

    name = domainroot.find("name").text
    vcpus = domainroot.find("vcpu").text
    memory = int(domainroot.find("memory").text)
    memorysize = round(memory*0.001024)
    diskxml = diskdir + "/" + name + ".disk.export.xml"
    if os.path.exists(diskxml):
        diskparse =  xmlet.parse(diskxml)
        diskroot = diskparse.getroot()
        diskcapacity = int(diskroot.find("capacity").text)
        disksize = round(diskcapacity / (math.pow(1024, (int(math.floor(math.log(diskcapacity, 1024)))))))
    if not os.path.exists(diskxml):
        print("No disk XML for " + name + ", assuming there is no VHD.")
        disksize = ""
    
    while True:
        role_choice = input ("Assign role for " + name + ":\n1) Internal Client\n2) Internal Server\n3) Public Client\n4) Public Server\n5) Customer\n6) Router\n7) Null\n> ")
        if role_choice not in ["1", "2", "3", "4", "5", "6", "7"]:
            print("Invalid choice.")
        if role_choice == "1":
            role = "Virtual Machine (Internal, Client)"
            break
        if role_choice == "2":
            role = "Virtual Machine (Internal, Server)"
            break
        if role_choice == "3":
            role = "Virtual Machine (Public, Client)"
            break
        if role_choice == "4":
            role = "Virtual Machine (Public, Server)"
            break
        if role_choice == "5":
            role = "Virtual Machine (Customer)"
            break
        if role_choice == "6":
            role = "Virtual Machine (Router)"
            break
        if role_choice == "7":
            role = ""
            break

    while True:
        platform_choice = input ("Assign platform for " + name + ":\n1) openSUSE Leap x86_64\n2) OpenBSD x86_64\n3) FreeBSD x86_64\n4) OPNsense x86_64\n5) Arch Linux x86_64\n7) Null\n> ")
        if platform_choice not in ["1", "2", "3", "4", "5", "7"]:
            print("Invalid choice.")
        if platform_choice == "1":
            platform = "openSUSE-Leap-x86_64"
            break
        if platform_choice == "2":
            platform = "OpenBSD-x86_64"
            break
        if platform_choice == "3":
            platform = "FreeBSD-x86_64"
            break
        if platform_choice == "4":
            platform = "OPNsense-x86_64"
            break
        if platform_choice == "5":
            platform = "ArchLinux-x86_64"
            break
        if platform_choice == "7":
            platform = ""
            break
        
    rows.append(
            {
                "name": name,
                "status": status,
                "role": role,
                "cluster": cluster,
                "tenant": tenant,
                "platform": platform,
                "vcpus": vcpus,
                "memory": memorysize,
                "disk": disksize,
                "comments": comment
            }
    )

convert = pandas.DataFrame(rows, columns=columns)
convert.to_csv(outfile, index=False)
