#!/usr/local/bin/python3.8
import requests
from git import Repo

organization = 'LibertaCasa'
URL = 'https://git.com.de/api/v1/orgs/' + organization + '/repos'

response = requests.get(
        URL,
        headers = {'accept': 'application/json'},
        )
data = response.json()
repo = []
for repos in data:
    reponame = repos['name']
    repourl = repos['ssh_url']
    print(reponame + ' ' + repourl)
    Repo.clone_from(repourl, reponame)
